package com.lagou.zhou.service.impl;

import com.lagou.mvcframework.annotations.Service;
import com.lagou.zhou.service.DemoService;
@Service
public class DemoServiceImpl implements DemoService {
    @Override
    public String toGet(String name) {
        return name+"进来了";
    }
}
