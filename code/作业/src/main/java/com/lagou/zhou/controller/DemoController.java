package com.lagou.zhou.controller;

import com.lagou.mvcframework.annotations.Autowired;
import com.lagou.mvcframework.annotations.Controller;
import com.lagou.mvcframework.annotations.RequestMapping;
import com.lagou.mvcframework.annotations.Security;
import com.lagou.zhou.service.DemoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/demo")
public class DemoController {

    @Autowired
    private DemoService demoService;

    @RequestMapping("/query")
    @Security({"lisi","zhangsan"})
    public String query(HttpServletRequest request, HttpServletResponse response, String name) {
        System.out.println(name+"/demo/query");
        return demoService.toGet(name);
    }

    @RequestMapping("/query2")
    @Security({"zhangsan"})
    public String query2(HttpServletRequest request, HttpServletResponse response, String name) {
        System.out.println(name+"/demo/query2");
        return demoService.toGet(name);
    }

    @RequestMapping("/query3")
    public String query3(HttpServletRequest request, HttpServletResponse response, String name) {
        System.out.println(name+"/demo/query3");
        return demoService.toGet(name);
    }


}
