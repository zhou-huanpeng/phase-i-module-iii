package com.lagou.mvcframework.servlet;

import com.lagou.mvcframework.annotations.*;
import com.lagou.mvcframework.domain.Handler;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DispatcherServlet extends HttpServlet {


    private Properties properties = new Properties();

    private List<String> classNames = new ArrayList<>();

    private Map<String, Object> ioc = new HashMap<>();

    private List<Handler> handlerMapping = new ArrayList<>();



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("utf-8");
        resp.setHeader("Content-Type", "text/html;charset=utf-8");

        Handler handler = getHandler(req);
        if (handler == null){
            resp.getWriter().write("404 NOT Found!");
            return;
        }

        if (handler.isFlag()){
            List<String> passNames = handler.getPassToken();
            Map<String, String[]> parameterMap = req.getParameterMap();
            for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
                String value = StringUtils.join(entry.getValue(), ",");
                if (!check(passNames,value)){
                    resp.getWriter().write("无权限！");
                    return;
                }

            }

        }

        //参数绑定
        //获取该方法的参数类型数组
        Class<?>[] parameterTypes = handler.getMethod().getParameterTypes();

        //创建一个新的数组
        Object[] objects = new Object[parameterTypes.length];

        //想参数数组中塞值
        for (Map.Entry<String, String[]> param : req.getParameterMap().entrySet()) {
            String value = StringUtils.join(param.getValue(), ",");
            if (!handler.getParamIndexMapping().containsKey(param.getKey())) continue;
            Integer index = handler.getParamIndexMapping().get(param.getKey());
            objects[index] = value;
        }

        Integer reqIndex = handler.getParamIndexMapping().get(HttpServletRequest.class.getSimpleName());
        objects[reqIndex] = req;
        Integer respIndex = handler.getParamIndexMapping().get(HttpServletResponse.class.getSimpleName());
        objects[respIndex] = resp;

        try {
            handler.getMethod().invoke(handler.getController(),objects);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    private Boolean check(List<String> passNames, String value) {
        for (String passName : passNames) {
            if (passName.equals(value)){
                return true;
            }
        }

        return false;
    }

    private Handler getHandler(HttpServletRequest req) {
        if (handlerMapping.isEmpty()) return null;

        String requestURI = req.getRequestURI();
        for (Handler handler : handlerMapping) {
            Matcher matcher = handler.getPattern().matcher(requestURI);
            if (!matcher.matches()){
                continue;
            }
            return handler;
        }

        return null;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        //初始化，加载配置文件
        String contextConfigLocation = config.getInitParameter("contextConfigLocation");
        doLoadConfig(contextConfigLocation);

        //扫描类，扫描注解
        doScan(properties.getProperty("scanPackage"));

        //初始化bean对象
        doInstance();

        //实现依赖注入
        doAutowired();

        //构造一个HandlerMapping处理器映射器，将url与方法建立映射关系

        initHandlerMapping();


        System.out.println("自定义MVC初始化完成=====");


    }

    private void initHandlerMapping() {
        if (ioc.isEmpty()) return;
        for (Map.Entry<String, Object> stringObjectEntry : ioc.entrySet()) {
            Class<?> aClass = stringObjectEntry.getValue().getClass();

            if (!aClass.isAnnotationPresent(Controller.class)) continue;
             String baseUrl = "";
             if (aClass.isAnnotationPresent(RequestMapping.class)){
                 RequestMapping annotation = aClass.getAnnotation(RequestMapping.class);
                 baseUrl = annotation.value();
             }

            for (Method method : aClass.getMethods()) {
                if (!method.isAnnotationPresent(RequestMapping.class)) continue;
                RequestMapping annotation = method.getAnnotation(RequestMapping.class);
                String value = annotation.value();
                String url = baseUrl + value;
                Handler handler = new Handler(stringObjectEntry.getValue(),method, Pattern.compile(url));
                if (method.isAnnotationPresent(Security.class) || aClass.isAnnotationPresent(Security.class)){
                    //开启权限认定
                    handler.setFlag(true);
                    List<String> passToken = new ArrayList<>();
                    if (method.isAnnotationPresent(Security.class) && !aClass.isAnnotationPresent(Security.class)){
                        //方法上有，类上没有
                        passToken.addAll(Arrays.asList(method.getAnnotation(Security.class).value()));
                    }else if (!method.isAnnotationPresent(Security.class) && aClass.isAnnotationPresent(Security.class)){
                        //类上有权限，方法上没有
                        passToken.addAll(Arrays.asList(aClass.getAnnotation(Security.class).value()));
                    }else if (method.isAnnotationPresent(Security.class) && aClass.isAnnotationPresent(Security.class)){
                        //类上有权限，方法上有
                        List<String> strings = Arrays.asList(method.getAnnotation(Security.class).value());
                        List<String> list = new ArrayList<>(strings);
                        list.addAll(Arrays.asList(aClass.getAnnotation(Security.class).value()));
                        //去重后加入
                        passToken.addAll( strings.stream().distinct().collect(Collectors.toList()));
                    }

                    handler.setPassToken(passToken);

                }

                //计算参数的位置信息
                Parameter[] parameters = method.getParameters();
                for (int i = 0; i < parameters.length; i++) {
                    Parameter parameter = parameters[i];
                    if (parameter.getType() == HttpServletRequest.class || parameter.getType() == HttpServletResponse.class){
                        handler.getParamIndexMapping().put(parameter.getType().getSimpleName(),i);
                    }else {
                        handler.getParamIndexMapping().put(parameter.getName(),i);
                    }
                }
                handlerMapping.add(handler);
            }
        }

    }

    private void doAutowired() {

        if (ioc.isEmpty()) return;

        for (Map.Entry<String, Object> stringObjectEntry : ioc.entrySet()) {
            Field[] declaredFields = stringObjectEntry.getValue().getClass().getDeclaredFields();
            for (Field declaredField : declaredFields) {
                if (! declaredField.isAnnotationPresent(Autowired.class)){
                    continue;
                }
                Autowired annotation = declaredField.getAnnotation(Autowired.class);
                String value = annotation.value();
                if ("".equals(value.trim())){
                    value = declaredField.getType().getName();
                }
               declaredField.setAccessible(true);

                try {
                    declaredField.set(stringObjectEntry.getValue(),ioc.get(value));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }


        }

    }

    private void doInstance() {
        if (classNames.size() == 0) return;
        try {
            for (int i = 0; i < classNames.size(); i++) {
                String className = classNames.get(i);
                Class<?> aClass = Class.forName(className);

                if (aClass.isAnnotationPresent(Controller.class)) {
                    ioc.put(lowerFirst(aClass.getSimpleName()), aClass.newInstance());
                } else if (aClass.isAnnotationPresent(Service.class)) {
                    Service annotation = aClass.getAnnotation(Service.class);
                    String value = annotation.value();
                    if (!"".equals(value.trim())) {
                        ioc.put(value, aClass.newInstance());
                    } else {
                        ioc.put(lowerFirst(aClass.getSimpleName()), aClass.newInstance());
                    }

                    Class<?>[] interfaces = aClass.getInterfaces();
                    for (Class<?> anInterface : interfaces) {
                        ioc.put(anInterface.getName(), aClass.newInstance());
                    }
                }else {
                    continue;
                }
            }
        } catch (Exception e) {
           e.printStackTrace();
        }

    }

    public String lowerFirst(String str) {
        char[] chars = str.toCharArray();
        if ('A' <= chars[0] && 'Z' >= chars[0]) {
            chars[0] += 32;
        }
        return String.valueOf(chars);
    }


    private void doScan(String scanPackage) {
        URL resource = Thread.currentThread().getContextClassLoader().getResource("");
        String path = resource.getPath();
        String s = scanPackage.replaceAll("\\.", "/");
        String scanPackagePath = path + s;
        File pack = new File(scanPackagePath);

        File[] files = pack.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                //如果是一个文件夹
                String s1 = scanPackage + "." + file.getName();
                doScan(s1);
            } else if (file.getName().endsWith(".class")) {
                String className = scanPackage + "." + file.getName().replaceAll(".class", "");
                classNames.add(className);
            }
        }


    }


    //加载配置文件
    private void doLoadConfig(String contextConfigLocation) {

        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(contextConfigLocation);
        try {
            properties.load(resourceAsStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
